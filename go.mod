module gitlab.com/k.mukhin/fleeting-plugin-yc

go 1.21

require (
	github.com/ghodss/yaml v1.0.0
	github.com/hashicorp/go-hclog v1.6.3
	github.com/yandex-cloud/go-genproto v0.0.0-20241007112235-3f73f279939e
	github.com/yandex-cloud/go-sdk v0.0.0-20241007112728-a06ce15e89c7
	gitlab.com/gitlab-org/fleeting/fleeting v0.0.0-20240911165028-a0ce7d6c3260
	golang.org/x/crypto v0.26.0
	google.golang.org/protobuf v1.34.2
)

require (
	github.com/fatih/color v1.17.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/hashicorp/go-plugin v1.6.1 // indirect
	github.com/hashicorp/yamux v0.1.1 // indirect
	github.com/jhump/protoreflect v1.15.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	github.com/oklog/run v1.1.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/net v0.28.0 // indirect
	golang.org/x/sys v0.24.0 // indirect
	golang.org/x/text v0.17.0 // indirect
	google.golang.org/genproto v0.0.0-20240903143218-8af14fe29dc1 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20240903143218-8af14fe29dc1 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240903143218-8af14fe29dc1 // indirect
	google.golang.org/grpc v1.66.2 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
